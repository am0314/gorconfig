package gorconfig

import (
	"encoding/json"
	"errors"
	"os"
	"reflect"

	"github.com/BurntSushi/toml"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

type Gonfig[C any] struct {
	Conf    C
	options Options
}

func NewGonfigDefault[C any](conf C, loggerOption *zap.Logger) (*Gonfig[C], error) {
	res := &Gonfig[C]{
		Conf: conf,
	}
	err := gorconfigMake(res.Conf, DefaultOptions(loggerOption))
	if err != nil {
		return nil, err
	}
	return res, nil
}

func NewGonfigOption[C any](conf C, option *Options) (*Gonfig[C], error) {
	res := &Gonfig[C]{
		Conf: conf,
	}
	err := gorconfigMake(res.Conf, option)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func NewGonfigAlternative[C any](conf C, option ...*Options) (*Gonfig[C], error) {
	if len(option) == 0 {
		return nil, errors.New("non option")
	}
	res := &Gonfig[C]{
		Conf: conf,
	}
	errPack := ""
	for k := range option {
		err := gorconfigMake(res.Conf, option[k])
		if err != nil {
			errPack += err.Error()
		}
		if len(errPack) != 0 && k == len(option)-1 {
			return nil, errors.New(errPack)
		}
	}

	return res, nil
}

func (g *Gonfig[T]) Save() error {
	switch g.options.SourceFrom {
	case SourceFromEnv:
		g.envSet(reflect.ValueOf(g.Conf), &g.options, new(int))
	case SourceFromFile:
		return g.formatSave()
	}
	return errors.New("SourceFrom not in case")
}

func (g *Gonfig[T]) envSet(ve reflect.Value, option *Options, topCount *int) error {
	for fieldCount := 0; fieldCount < ve.NumField(); fieldCount++ {
		*topCount++
		if *topCount == option.MaxFieldOption {
			return errors.New(`
			the number of parsing exceeds the limit,
			 maybe a circular reference?
			  If your profile is very large,
			   please adjust option.MaxFieldOption
			`)
		}
		rangeField := ve.Field(fieldCount)
		switch rangeField.Kind() {
		case reflect.Struct:
			err := g.envSet(rangeField, &g.options, topCount)
			if err != nil {
				return err
			}
		default:
			if err := os.Setenv(rangeField.String(), rangeField.String()); err != nil {
				return err
			}
		}
	}
	return nil
}

func (g *Gonfig[T]) formatSave() error {
	switch g.options.SourceFormat {
	case SourceFormatJson:
		if err := json.NewEncoder(g.options.fs).Encode(g.Conf); err != nil {
			return err
		}
	case SourceFormatToml:
		if err := toml.NewEncoder(g.options.fs).Encode(g.Conf); err != nil {
			return err
		}
	case SourceFormatYaml, SourceFormatYml:
		if err := yaml.NewEncoder(g.options.fs).Encode(g.Conf); err != nil {
			return err
		}
	}
	return errors.New("SourceFormat not in case")
}
