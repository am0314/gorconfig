package gorconfig

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"reflect"
	"strconv"
	"strings"

	"github.com/BurntSushi/toml"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

// Use default setting to get set value,
// [from:env] [format:raw] [logger:zap.NewDevelopment]
func DefaultParse(configObj any, loggerOption *zap.Logger) error {
	return gorconfigMake(configObj, DefaultOptions(loggerOption))
}

func OptionsParse(configObj any, option Options) error {
	if err := option.checkSelf(); err != nil {
		return err
	}
	return gorconfigMake(configObj, &option)
}

func MakeDefaultConfigFile(configObj any, option *Options) (*os.File, error) {
	if err := option.checkSelf(); err != nil {
		return nil, err
	}

	return makeDefault(configObj, option)
}

func gorconfigMake(configObj any, option *Options) (err error) {
	masterValue := reflect.ValueOf(configObj)
	if masterValue.Kind() != reflect.Pointer {
		panic("target is not a pointer")
	}
	if masterValue.Elem().Kind() != reflect.Struct {
		panic("target is not a struct")
	}
	switch option.SourceFrom {
	case SourceFromEnv:
		return envFill(masterValue.Elem(), option, new(int))

	case SourceFromFile:
		option.fs, err = os.Open(option.FilePath)
		if err != nil {
			if option.MakeDefault {
				if _, err := makeDefault(configObj, option); err != nil {
					failFmt := "fail to create config file paht: %s , error message: %s"
					option.LoggerOption.Sugar().Fatalf(failFmt, option.FilePath, err.Error())
				}
				os.Exit(0)
			}
			return err
		}

		bDate, err := io.ReadAll(option.fs)
		if err != nil {
			return err
		}
		return dataFile(configObj, bDate, option)
	default:
		return fmt.Errorf("unsupported source %s", option.SourceFrom)
	}
}

func makeDefault(configObj any, option *Options) (*os.File, error) {
	fs, err := os.Create(option.FilePath)
	if err != nil {
		return nil, err
	}
	switch option.SourceFormat {
	case SourceFormatJson:
		if json.NewEncoder(fs).Encode(configObj); err != nil {
			return nil, err
		}
	case SourceFormatToml:
		if err := toml.NewEncoder(fs).Encode(configObj); err != nil {
			return nil, err
		}

	case SourceFormatYaml, SourceFormatYml:
		if err := yaml.NewEncoder(fs).Encode(configObj); err != nil {
			return nil, err
		}

	default:
		return nil, fmt.Errorf("unsupported format %s", option.SourceFormat)
	}

	option.LoggerOption.Info(fmt.Sprintf("success make default config in path: %s", option.FilePath))
	return fs, nil
}

func dataFile(configObj any, bDate []byte, option *Options) error {
	switch option.SourceFormat {
	case SourceFormatJson:
		if err := json.Unmarshal(bDate, configObj); err != nil {
			return err
		}

	case SourceFormatYaml, SourceFormatYml:
		if err := yaml.Unmarshal(bDate, configObj); err != nil {
			return err
		}
	case SourceFormatToml:
		_, err := toml.Decode(string(bDate), configObj)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("unsupported format %s", option.SourceFormat)
	}
	return nil
}

func envFill(ve reflect.Value, option *Options, topCount *int) error {
	var isAllEmpty = true
	for fieldCount := 0; fieldCount < ve.NumField(); fieldCount++ {
		*topCount++
		if *topCount == option.MaxFieldOption {
			return errors.New(`
			the number of parsing exceeds the limit,
			 maybe a circular reference?
			  If your profile is very large,
			   please adjust option.MaxFieldOption
			`)
		}

		rangeField := ve.Field(fieldCount)

		callSing := getCallSing(ve, fieldCount)
		callValue := os.Getenv(callSing)
		if callValue == "" && rangeField.Kind() != reflect.Struct {
			option.LoggerOption.Warn(fmt.Sprintf("call from env [%s] fail , it's empty", callSing))
			continue
		}
		isAllEmpty = false

		switch rangeField.Kind() {
		// case reflect.Array:
		case reflect.String:
			rangeField.SetString(callValue)
		case reflect.Int8:
			i, err := strconv.ParseInt(callValue, 10, 8)
			if err != nil {
				return err
			}
			rangeField.SetInt(i)
		case reflect.Int:
			i, err := strconv.ParseInt(callValue, 10, 32)
			if err != nil {
				return err
			}
			rangeField.SetInt(i)

		case reflect.Int16:
			i, err := strconv.ParseInt(callValue, 10, 16)
			if err != nil {
				return err
			}
			rangeField.SetInt(i)
		case reflect.Int32:
			i, err := strconv.ParseInt(callValue, 10, 32)
			if err != nil {
				return err
			}
			rangeField.SetInt(i)
		case reflect.Int64:
			i, err := strconv.ParseInt(callValue, 10, 64)
			if err != nil {
				return err
			}
			rangeField.SetInt(i)
		case reflect.Float32:
			i, err := strconv.ParseFloat(callValue, 32)
			if err != nil {
				return err
			}
			rangeField.SetFloat(i)
		case reflect.Float64:
			i, err := strconv.ParseFloat(callValue, 64)
			if err != nil {
				return err
			}
			rangeField.SetFloat(i)
		case reflect.Uint:
			i, err := strconv.ParseUint(callValue, 10, 32)
			if err != nil {
				return err
			}
			rangeField.SetUint(i)
		case reflect.Uint8:
			i, err := strconv.ParseUint(callValue, 10, 8)
			if err != nil {
				return err
			}
			rangeField.SetUint(i)
		case reflect.Uint16:
			i, err := strconv.ParseUint(callValue, 10, 16)
			if err != nil {
				return err
			}
			rangeField.SetUint(i)
		case reflect.Uint32:
			i, err := strconv.ParseUint(callValue, 10, 32)
			if err != nil {
				return err
			}
			rangeField.SetUint(i)
		case reflect.Uint64:
			i, err := strconv.ParseUint(callValue, 10, 64)
			if err != nil {
				return err
			}
			rangeField.SetUint(i)
		case reflect.Bool:
			i, err := strconv.ParseBool(callValue)
			if err != nil {
				return err
			}
			rangeField.SetBool(i)
		case reflect.Slice:
			callValueArray := strings.Split(callValue, option.EnvArraySplitSymbol)
			if len(callValueArray) < 1 || callValue == "" {
				continue
			}
			err := fillArray(rangeField, callValueArray, callValue)
			if err != nil {
				return err
			}

		case reflect.Struct:
			if err := envFill(rangeField, option, topCount); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unsupported type %s", rangeField.Kind().String())
		}
	}
	if isAllEmpty {
		return errors.New("all env not find value")
	}

	return nil
}

func fillArray(rangeField reflect.Value, callValueArray []string, callValue string) error {
	tmpTof := reflect.TypeOf(rangeField)
	reflect.SliceOf(tmpTof).Kind()

	if rangeField.Len() < len(callValueArray) {
		rangeField.SetCap(len(callValueArray))
		rangeField.SetLen(len(callValueArray))
	}

	slicetype := rangeField.Type().Elem().Kind()
	for k := range callValueArray {
		switch slicetype {
		case reflect.String:
			rangeField.Index(k).SetString(callValueArray[k])
		case reflect.Int:
			i, err := strconv.ParseInt(callValue, 10, 32)
			if err != nil {
				return err
			}
			rangeField.Index(k).SetInt(i)
		case reflect.Float64:
			i, err := strconv.ParseFloat(callValue, 64)
			if err != nil {
				return err
			}
			rangeField.Index(k).SetFloat(i)
		case reflect.Bool:
			i, err := strconv.ParseBool(callValue)
			if err != nil {
				return err
			}
			rangeField.Index(k).SetBool(i)
		default:
			return errors.New("array only supper String,Int,Float64,Bool")
		}
	}
	return nil
}

func getCallSing(value reflect.Value, fieldCount int) string {
	e, b := value.Type().Field(fieldCount).Tag.Lookup("gorconfig")
	if !b {
		return value.Type().Field(fieldCount).Name
	}
	return e
}
