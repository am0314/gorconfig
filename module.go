package gorconfig

import (
	"errors"
	"os"

	"go.uber.org/zap"
)

type SourceFrom string

const (
	SourceFromEnv  SourceFrom = "env"
	SourceFromFile SourceFrom = "file"
)

type SourceFormat string

const (
	SourceFormatJson SourceFormat = "json"
	SourceFormatYaml SourceFormat = "yaml"
	SourceFormatYml  SourceFormat = "yml"
	SourceFormatToml SourceFormat = "toml"

	// SourceFormatRaw SourceFormat = "raw"
)

type Options struct {
	// is config source is from env or file?
	SourceFrom SourceFrom
	// support json yaml yml toml , only works on SourceFromFile
	SourceFormat SourceFormat
	// file open path , or create path
	FilePath string
	// gorconfig need to export log , is nil , default zap.NewExample
	LoggerOption *zap.Logger
	// only work on SourceFromFile
	// if true , when source from file and open fail
	// will create conf.(SourceFormat) in FilePath ,
	// and fatalf when fail , exit on success
	MakeDefault bool

	// if your conf is very very big , you need to set to big number,
	// otherwise not need to set , defult is 1024 , prevent loops
	MaxFieldOption int

	// if you conf call from env, and has array
	// set this field to Split , defult Split is ","
	EnvArraySplitSymbol string

	fs *os.File
}

func (o *Options) checkSelf() error {
	if o.LoggerOption == nil {
		o.LoggerOption = zap.NewExample()
	}
	if o.SourceFrom == SourceFromFile && o.FilePath == "" {
		return errors.New("path is empty")
	}

	if o.MaxFieldOption == 0 {
		o.MaxFieldOption = 1024
	}
	if o.EnvArraySplitSymbol == "" {
		o.EnvArraySplitSymbol = ","
	}

	return nil
}

func DefaultOptions(logger *zap.Logger) *Options {
	if logger == nil {
		logger = zap.NewExample()
	}
	return &Options{
		SourceFrom:   SourceFromEnv,
		SourceFormat: "",
		FilePath:     "",
		LoggerOption: logger,
		MakeDefault:  false,
	}
}
