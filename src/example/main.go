package main

import (
	"fmt"

	"gitlab.com/am0314/gorconfig"
	"go.uber.org/zap"
)

type TestServerSetting struct {
	DBAccount  string `gorconfig:"USER_ACCOUNT"`
	DBPassword string
	ListenAddr string
	ListenPort int
	Day        []string
	TmpStr     TmpStr
}

type TmpStr struct {
	Com   uint
	Heave bool
}

func main() {
	exampleNewGonfigOption()
	// exampleFromDefaultParse()
}

func exampleNewGonfigOption() {
	logger := zap.NewExample()

	// logger, err := zap.NewNop(), nil
	// if err != nil {
	// 	return
	// }
	target := &TestServerSetting{
		DBAccount:  "",
		DBPassword: "",
		ListenAddr: "",
		ListenPort: 0,
	}
	opt := &gorconfig.Options{
		SourceFrom:   gorconfig.SourceFromFile,
		SourceFormat: gorconfig.SourceFormatToml,
		FilePath:     "./example.toml",
		LoggerOption: logger,
		MakeDefault:  true,
	}
	_, err := gorconfig.NewGonfigDefault(&TestServerSetting{}, logger)
	if err != nil {
		logger.Sugar().Info(err)
	}

	mconf, err := gorconfig.NewGonfigOption(target, opt)
	if err != nil {
		logger.Warn(err.Error())
		return
	}
	fmt.Printf("%+v", mconf.Conf)
}

func exampleFromDefaultParse() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		return
	}
	target := &TestServerSetting{
		DBAccount:  "",
		DBPassword: "",
		ListenAddr: "",
		ListenPort: 0,
	}
	if err = gorconfig.DefaultParse(target, nil); err != nil {
		logger.Sugar().Info(err)
	}

	if err = gorconfig.OptionsParse(target, gorconfig.Options{
		SourceFrom:   gorconfig.SourceFromFile,
		SourceFormat: gorconfig.SourceFormatToml,
		FilePath:     "./example.toml",
		LoggerOption: logger,
		MakeDefault:  true,
	}); err != nil {
		logger.Sugar().Fatal(err)
	}

	fmt.Printf("err=%+v conf= %+v \n", err, target)
}
