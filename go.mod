module gitlab.com/am0314/gorconfig

go 1.19

require (
	github.com/BurntSushi/toml v1.2.0
	go.uber.org/zap v1.22.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
)
